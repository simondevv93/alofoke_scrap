<?php

class Database {
    private $host = '127.0.0.1:5566';
    private $port = '5566';
    private $db_name = 'alofoke_scrap_db';
    private $username = 'root';
    private $password = 'password';
    public $conn;

    public function getConnection(){
        
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8mb4");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}
