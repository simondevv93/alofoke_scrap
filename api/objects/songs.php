<?php

class Product {

    private $conn;
    private $table_name = "crawler_songs";

    public $id;
    public $artist_name;
    public $song_name;
    public $picture_url;
    public $song_url;
    public $date_verified;
    public $date_inserted;
    public $src;
    public $dest;
    public $related_id;
    public $related_date;
    public $related_user;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    public function getSongs()
    {
        $limit = 0;

        if(isset($_GET['limit'])){
            $limit = $_GET['limit'];
        }else {
            $limit = 10000;
        }

        if(!isset($_GET['page'])){
            $page = 1;
        }else {
            $page = $_GET['page'];
        }

        $query = "SELECT * from alofoke_scrap_db.crawler_songs;";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $total_results = $stmt->rowCount();
        $total_pages = ceil($total_results / $limit);
        
        $starting_limit = ($page-1)*$limit;
        $show = "SELECT * from alofoke_scrap_db.crawler_songs ORDER BY id LIMIT $starting_limit, $limit";
        $r = $this->conn->prepare($show);
        $r->execute();

        return [
            $r,
            $total_pages,
            $total_results
        ];
    }

    public function getPending()
    {
        $limit = 0;

        if(isset($_GET['limit'])){
            $limit = $_GET['limit'];
        }else {
            $limit = 10000;
        }

        if(!isset($_GET['page'])){
            $page = 1;
        }else {
            $page = $_GET['page'];
        }

        $query = "SELECT * from alofoke_scrap_db.crawler_songs;";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $total_results = $stmt->rowCount();
        $total_pages = ceil($total_results / $limit);

        $starting_limit = ($page-1)*$limit;

        $show = "SELECT * from alofoke_scrap_db.crawler_songs where related_date IS NULL ORDER BY id LIMIT $starting_limit, $limit;";

        $r = $this->conn->prepare($show);
        $r->execute();

        return $r;

    }

    public function relate()
    {

        error_log($this->related_user);
        if($this->related_id == "null"  && $this->related_user == "null" && $this->related_date == "null" || $this->related_user == "" && $this->related_id == "" && $this->related_date == "") {
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_date = NULL, related_id = NULL, related_user = NULL where id = :id";
            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);

        }else if($this->related_id == "null" && $this->related_user == "null" || $this->related_user == "" && $this->related_id == "" || $this->related_id == "null"  && $this->related_date == "null"){
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_date = :related_date, related_id = NULL, related_user = NULL where id = :id";
            error_log('aquii');

            $stmt = $this->conn->prepare($query);
            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_date', $this->related_date);


        }else if($this->related_user == "null" && $this->related_date == "null" || $this->related_user == "" && $this->related_date == "") {
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_user = null, related_date = NULL, related_id = :related_id where id = :id";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_id', $this->related_id);

            error_log('cool4');
        }else if($this->related_date == "null" || $this->related_date == "" && $this->related_id == "null" || $this->related_id == "") {
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_user = :related_user, related_date = NULL, related_id = NULL where id = :id";
            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_user', $this->related_user);

            error_log('cool3');
        }else if($this->related_user == "null" || $this->related_user == ""){

            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_user = NULL, related_date = :related_date, related_id = :related_id where id = :id";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_id', $this->related_id);
            $stmt->bindParam(':related_date', $this->related_date);

            error_log('cool');

        }else if($this->related_id == "null" || $this->related_id == ""){
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_user = :related_user, related_date = :related_date, related_id = NULL where id = :id";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_date', $this->related_date);
            $stmt->bindParam(':related_user', $this->related_user);

            error_log("cool22");

        }else if($this->related_date == "null" || $this->related_date == ""){
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_user = :related_user, related_date = NULL, related_id = :related_id where id = :id";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_user', $this->related_user);
            $stmt->bindParam(':related_id', $this->related_id);


            error_log($this->related_date);

        }else{
            $query = "UPDATE alofoke_scrap_db.crawler_songs SET related_user = :related_user, related_date = :related_date, related_id = :related_id where id = :id";

            $stmt = $this->conn->prepare($query);

            $stmt->bindParam(':id', $this->id);
            $stmt->bindParam(':related_id', $this->related_id);
            $stmt->bindParam(':related_date', $this->related_date);
            $stmt->bindParam(':related_user', $this->related_user);

            error_log('full thing');
        }

        if($stmt->execute()){
            return true;
        }else {
            return false;
        }
    }
}