<?php
header("Access-Controll-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../config/database.php';
include_once '../objects/songs.php';

$database = new Database();
$db = $database->getConnection();

$songs = new Product($db);

$stmt = $songs->getPending();
$num = $stmt->rowCount();

if($num>0){

    $songs_arr = array();
    $songs_arr["records"] = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        extract($row);

        $song_item = array(
            "id" => $id,
            "artist_name" => $artist_name,
            "song_name" => $song_name,
            "picture_url" => $picture_url,
            "song_url" => $song_url,
            "date_verified" => $date_verified,
            "date_inserted" => $date_inserted,
            "src" => $src,
            "dest" => $dest,
            "related_id" => $related_id,
            "related_date" => $related_date,
            "related_user" => $related_user
        );

        array_push($songs_arr["records"], $song_item);

    }

    http_response_code(200);

    echo json_encode($songs_arr);

}else{
    http_response_code(404);

    echo json_encode(
        array("Message" => "No songs found.")
    );
}