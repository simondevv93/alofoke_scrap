<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8mb4");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../config/database.php';
include_once '../objects/songs.php';

$database = new Database();
$db = $database->getConnection();

$songs = new Product($db);

$data = json_decode(file_get_contents("php://input"));
error_log(json_encode($data));
if(!empty($data->id)){
    $songs->id = $data->id;
    $songs->related_id = $data->related_id;
    $songs->related_date = $data->related_date;
    $songs->related_user = $data->related_user;

    if($songs->relate()){
        http_response_code(200);
        error_log('done');
        echo json_encode(array(
            "msg" => 200,
            "message" => "Song related."
        ));
    }else{
          // set response code - 503 service unavailable
        http_response_code(503);
    
        // tell the user
        echo json_encode(array("message" => "Unable to relate song."));
    }
}